package com.msucil.demo.spring.configuration;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import org.apache.commons.dbcp2.BasicDataSource;

/**
 * Configuration class
 * Configures Test Environment for DAO
 *
 * @author Sushil Ale
 * @since 7/6/2017
 */

@Configuration
@PropertySource("classpath:test-db.properties")
@ComponentScan(basePackages = "com.msucil.demo.spring.domain.entity")
@EnableTransactionManagement
public class JpaTestConfiguration {

    private final Environment env;

    @Autowired
    public JpaTestConfiguration(Environment env){
        this.env = env;
    }


    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(){
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();

        final JpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();

        em.setJpaVendorAdapter(jpaVendorAdapter);
        em.setDataSource(dataSource());
        em.setJpaProperties(hibernateProperties());
        em.setPackagesToScan("com.msucil.demo.spring.domain");

        return em;
    }

    /*@Bean
    public EntityManager entityManager(EntityManagerFactory entityManagerFactory){
        return entityManagerFactory.createEntityManager();
    }*/

    /*@Bean
    public LocalSessionFactoryBean sessionFactory() {
        final LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();

        sessionFactory.setDataSource(dataSource());
        sessionFactory.setHibernateProperties(hibernateProperties());
        sessionFactory.setPackagesToScan("com.msucil.demo.spring.domain.entity");

        return sessionFactory;
    }*/

    @Bean
    public DataSource dataSource(){
        final BasicDataSource dataSource = new BasicDataSource();

        dataSource.setDriverClassName(env.getProperty("test.jdbc.driverClassName"));
        dataSource.setUrl(env.getProperty("test.jdbc.url"));
        dataSource.setUsername("test.jdbc.username");
        dataSource.setPassword("test.jdbc.password");

        return dataSource;
    }

    private Properties hibernateProperties() {
        return new Properties(){
            {
                setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
                setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
                setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
                setProperty("hibernate.format_sql", env.getProperty("hibernate.format_sql"));
                setProperty("hibernate.globally_quoted_identifiers", "true");
            }
        };
    }

    @Bean
    public PlatformTransactionManager transactionManager (EntityManagerFactory emf) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;

    }

    /*@Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory){
        final HibernateTransactionManager txManager = new HibernateTransactionManager();

        txManager.setSessionFactory(sessionFactory);

        return txManager;
    }*/

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslationPostProcessor(){
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
