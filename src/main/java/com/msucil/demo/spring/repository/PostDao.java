package com.msucil.demo.spring.repository;

import com.msucil.demo.spring.domain.entity.Post;

/**
 * @author Sushil Ale
 * @since 7/18/2017
 */
public interface PostDao extends BaseDao<Post, Long> {
}
