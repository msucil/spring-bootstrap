package com.msucil.demo.spring.repository;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;

import java.time.LocalDate;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.msucil.demo.spring.configuration.AbstractTransactionalDaoTestContext;
import com.msucil.demo.spring.domain.constant.Status;
import com.msucil.demo.spring.domain.entity.Post;
import com.msucil.demo.spring.domain.entity.PostCategory;
import org.testng.annotations.Test;

/**
 * @author Sushil Ale
 * @since 9/8/2017
 */
@DatabaseSetup("/test-dataset/post/post-with-categories.xml")
public class PostCategoryDaoImplTest extends AbstractTransactionalDaoTestContext {

    @Autowired
    private PostCategoryDao postCategoryDao;

    @Autowired
    private PostDao postDao;

    @Test
    public void testSave() {
        final PostCategory postCategory = new PostCategory();
        postCategory.setName("test category");
        postCategory.setSlug("test-category");
        postCategory.setDescription("test category description");
        postCategory.setStatus(Status.DRAFT);
        postCategory.setCreatedAt(LocalDate.now());
        postCategory.setCreatedBy(1L);

        final PostCategory savedCategory = postCategoryDao.save(postCategory);

        savedCategory.setName("testing 2");

        postCategoryDao.update(savedCategory);

        assertThat(savedCategory, is(notNullValue()));
        assertThat(savedCategory.getId(), is(any(Long.class)));
        assertThat(savedCategory.getName(), is(equalTo(postCategory.getName())));
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void testSaveThrowsExceptionOnSavingWithoutNotNullProperty() {
        final PostCategory postCategory = new PostCategory();
        postCategory.setName("test category");
        postCategory.setSlug("test-category");
        postCategory.setDescription("test category description");
        postCategory.setCreatedAt(LocalDate.now());

        postCategoryDao.save(postCategory);
    }

    @Test
    public void testFindReturnsPostCategory() {
        final PostCategory postCategory = postCategoryDao.find(1L);

        assertThat(postCategory, is(notNullValue()));
        assertThat(postCategory.getName(), is(equalTo("test category 1")));
    }

    @Test
    public void testFindReturnsPostCategoryWithPost() {
        final PostCategory postCategory = postCategoryDao.find(2L);

        assertThat(postCategory, is(notNullValue()));
        assertThat(postCategory.getName(), is(equalTo("test category 55")));
        assertThat(postCategory.getPosts(), hasSize(2));
    }

    @Test
    public void testUpdateRetursUpdatedPostCategory() {
        final PostCategory category  = postCategoryDao.find(1L);

        category.setName("testing category");

        final PostCategory updatedCategory = postCategoryDao.update(category);

        assertThat(updatedCategory.getName(), is(equalTo("testing category")));
    }

    @Test
    public void testDeleteCategoryByIdRemoveLinkedPost() {
        postCategoryDao.deleteById(2L);

        final QueryResult<Post> queryResult = postDao.findAll(null, 1, 10);

        assertThat(queryResult.getTotalCount(), is(equalTo(1L)));
    }

}
