package com.msucil.demo.spring.repository;

import lombok.Getter;

import org.apache.commons.lang3.StringUtils;

/**
 * Enum for specifying Sorting Order used in query
 *
 * @author Sushil Ale
 * @since 8/29/2017
 */
public enum SortOrder {
    ASC("asc"), DESC("desc");

    @Getter
    private final String value;

    SortOrder(String direction) {
        value = direction;
    }

    public static SortOrder getByValue(String value) {

        for (SortOrder order : values()) {
            if (StringUtils.equals(order.getValue(), value)) {
                return order;
            }
        }

        return null;
    }
}
