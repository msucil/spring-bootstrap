package com.msucil.demo.spring.configuration;

import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.dataset.FlatXmlDataSetLoader;
import com.msucil.demo.spring.configuration.AbstractTransactionalDaoTestContext.NullDatasetLoader;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;

/**
 * DAO Test configuration class
 * Mendatory to test DAO layer
 * Configures DbUnit and SpringContext for Test
 *
 * @author Sushil Ale
 * @since 8/1/2017
 */
@ContextConfiguration(classes = DaoTestContext.class)
@Transactional
@TestExecutionListeners({
    DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
@DbUnitConfiguration(dataSetLoader = NullDatasetLoader.class)
public abstract class AbstractTransactionalDaoTestContext extends AbstractTransactionalTestNGSpringContextTests {

    public static class NullDatasetLoader extends FlatXmlDataSetLoader {

        @Override
        protected IDataSet createDataSet(Resource resource) throws Exception {
            final ReplacementDataSet dataSet = new ReplacementDataSet(super.createDataSet(resource));

            dataSet.addReplacementObject("[null]", null);

            return dataSet;
        }
    }
}
