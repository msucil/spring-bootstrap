package com.msucil.demo.spring.repository;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Wrapper class for query result
 * Wraps list of query result and total count of the query
 * Used for pagination
 *
 * @author Sushil Ale
 * @since 8/29/2017
 */

@Getter
@NoArgsConstructor
public class QueryResult<T> implements Serializable {

    private static final long serialVersionUID = -2265833356647218708L;
    private List<T> result;

    @Setter
    private Long totalCount;

    public void setResult(List<T> result) {
        this.result = result;
    }
}
