package com.msucil.demo.spring.domain.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Sushil Ale
 * @since 7/18/2017
 */
@Entity
@Table(name = "post")
@Data
@EqualsAndHashCode(callSuper = true)
public class Post extends Article {
    private static final long serialVersionUID = 5317459781936518960L;

    @JoinColumn(name = "post_category_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private @Valid PostCategory postCategory;
}
