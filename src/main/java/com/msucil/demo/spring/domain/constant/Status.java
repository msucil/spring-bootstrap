package com.msucil.demo.spring.domain.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Sushil Ale
 * @since 7/10/2017
 */
@AllArgsConstructor
public enum Status {
    DRAFT("DRAFT"), PUBLISHED("PUBLISHED"), ARCHIEVE("ARCHIEVE");

    @Getter
    private String value;

    public static Status getByValue(String value) {
        for (Status status : values()) {
            if (status.getValue().equals(value)) {
                return status;
            }
        }

        return null;
    }
}
