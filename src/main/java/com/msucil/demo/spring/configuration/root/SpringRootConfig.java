package com.msucil.demo.spring.configuration.root;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;

/**
 * @author Sushil Ale
 * @since 6/29/2017
 */

@Configuration
@Import(JpaConfig.class)
@ComponentScan(basePackages = "com.msucil.demo.spring",
    excludeFilters = @Filter(type = FilterType.REGEX,
        pattern = "com.msucil.demo.spring.web.*"))
public class SpringRootConfig {
}
