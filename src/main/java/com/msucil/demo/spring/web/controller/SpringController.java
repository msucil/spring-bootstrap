package com.msucil.demo.spring.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Sushil Ale
 * @since 6/29/2017
 */

@Controller
@Slf4j
public class SpringController {

    @RequestMapping("/")
    public String indexPage() {
        return "index";
    }

    @GetMapping("/thymeleaf")
    public String thymeleafPage() {
        return "thpage";
    }

}
