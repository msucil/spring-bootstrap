package com.msucil.demo.spring.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Configuratoin class for DAO Test
 *
 * @author Sushil Ale
 * @since 8/1/2017
 *
 */

@Configuration
@ComponentScan(basePackages = "com.msucil.demo.spring.repository")
@Import(JpaTestConfiguration.class)
public class DaoTestContext {

}
