package com.msucil.demo.spring.repository;

import com.msucil.demo.spring.domain.entity.Post;

/**
 * @author Sushil Ale
 * @since 8/29/2017
 */
public class PostQueryResult extends QueryResult<Post> {
}
