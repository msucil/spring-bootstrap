package com.msucil.demo.spring.repository;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.testng.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.msucil.demo.spring.configuration.AbstractTransactionalDaoTestContext;
import com.msucil.demo.spring.domain.constant.Status;
import com.msucil.demo.spring.domain.entity.Post;
import com.msucil.demo.spring.domain.entity.PostCategory;
import org.testng.annotations.Test;

/**
 * @author Sushil Ale
 * @since 8/1/2017
 */
@DatabaseSetup("/test-dataset/post/post-with-categories.xml")
public class PostDaoImplTest extends AbstractTransactionalDaoTestContext {

    @Autowired
    private PostDao postDao;

    @Autowired
    private PostCategoryDao postCategory;

    @Test
    public void testSave() throws Exception {

        final Post post = new Post();
        post.setTitle("test post with");
        post.setSlug("test-post-with");
        post.setContent("hello world, testing post.");
        post.setCreatedAt(LocalDate.now());
        post.setCreatedBy(1L);
        post.setStatus(Status.PUBLISHED);

        post.setPostCategory(postCategory.find(2L));

        postDao.save(post);

        assertNotNull(post.getId());
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void testSaveThrowsException() {
        final Post post = new Post();

        post.setTitle("test post");
        post.setSlug("test-post");
        post.setContent("hello world, testing post.");
        post.setCreatedBy(1L);
        post.setCreatedAt(LocalDate.now());

        postDao.save(post);
    }

    @Test
    public void testGetPostReturnPost() {
        final Post post = postDao.find(1L);

        assertNotNull(post);
        assertThat(post.getId(), is(equalTo(1L)));
        assertThat(post.getTitle(), is(equalTo("test post")));
        assertThat(post.getStatus(), is(equalTo(Status.DRAFT)));
        assertThat(post.getPostCategory(), is(not(nullValue())));
        assertThat(post.getPostCategory().getName(), is(equalTo("test category 55")));
    }

    @Test
    public void testUpdate() {
        final Post post = postDao.find(1L);
        post.setTitle("yello");
        post.setStatus(Status.ARCHIEVE);

        final Post changedPost = postDao.update(post);

        assertThat(changedPost.getTitle(), is(equalTo(post.getTitle())));
        assertThat(changedPost.getStatus(), is(equalTo(post.getStatus())));
    }

    @Test
    public void testDeleteByEntityLeavesCategory() {
        final Post post = postDao.find(1L);

        assertNotNull(post);

        postDao.delete(post);

        final Map<String, SortOrder> orderBy = new HashMap<>();
        orderBy.put("title", SortOrder.ASC);

        final QueryResult<Post> queryResult = postDao.findAll(orderBy, 1, 10);

        assertThat(queryResult.getResult(), hasSize(2));

        final QueryResult<PostCategory> categoryQueryResult = postCategory.findAll(null, 1, 0);

        assertThat(categoryQueryResult.getResult(), hasSize(3));
    }

    @Test
    public void testDeleteById() throws Exception {
        postDao.deleteById(2L);

        final Map<String, SortOrder> orderBy = new HashMap<>();
        orderBy.put("title", SortOrder.ASC);

        final QueryResult<Post> queryResult = postDao.findAll(orderBy, 1, 10);

        assertThat(queryResult.getResult(), hasSize(2));
    }

    @Test
    public void testFindOneReturnsOnePost() {
        final Post post = postDao.find(2L);

        assertNotNull(post);
        assertThat(post.getId(), is(equalTo(2L)));
    }

    @Test
    public void testFindAllReturnsAllPost() {
        final QueryResult<Post> queryResult = postDao.findAll(null, 1, 0);

        assertThat(queryResult.getResult(), hasSize(3));
    }

    @Test
    public void testFindAllRetunsPostsInChunk() {
        QueryResult<Post> queryResult = postDao.findAll(null, 1, 2);

        assertThat(queryResult.getResult(), is(notNullValue()));
        assertThat(queryResult.getResult(), hasSize(2));
        assertThat(queryResult.getTotalCount(), is(equalTo(3L)));

        queryResult = postDao.findAll(null,2, 2);
        assertThat(queryResult.getResult(), is(notNullValue()));
        assertThat(queryResult.getResult(), hasSize(1));
        assertThat(queryResult.getTotalCount(), is(equalTo(3L)));

    }

    @Test
    public void testFindAllRetunsNullOnExceedingPage() {
        final QueryResult<Post> queryResult = postDao.findAll(null, 3, 2);

        assertThat(queryResult.getResult(), is(notNullValue()));
        assertThat(queryResult.getResult(), hasSize(0));
        assertThat(queryResult.getTotalCount(), is(equalTo(3L)));
    }

    @Test
    public void testFindOneByRetunsSinglePostForTitleParameter() {
        final Map<String, Object> attributeValues = new HashMap<>();
        attributeValues.put("title", "test post 33");

        final Optional<Post> result = postDao.findOneBy(attributeValues);

        assertThat(result.isPresent(), is(equalTo(true)));
        result.ifPresent(post -> assertThat(post.getTitle(), is(equalTo(attributeValues.get("title")))));
    }

    @Test
    public void testFindOneByRetunsSinglePostForTitleAndSlugParameter() {
        final Map<String, Object> restrictionsParameter = new HashMap<>();
        restrictionsParameter.put("title", "test post 33");
        restrictionsParameter.put("slug", "test-post-3");

        final Optional<Post> result = postDao.findOneBy(restrictionsParameter);

        assertThat(result.isPresent(), is(equalTo(true)));
        result.ifPresent(post -> {
            assertThat(post.getTitle(), is(equalTo(restrictionsParameter.get("title"))));
            assertThat(post.getSlug(), is(equalTo(restrictionsParameter.get("slug"))));
        });
    }

    @Test
    public void testFindOneByRetunsNullForUnmatchedParameter() {
        final Map<String, Object> restrictionParameter = new HashMap<>();
        restrictionParameter.put("title", "test post 100");

        final Optional<Post> result = postDao.findOneBy(restrictionParameter);

        assertThat(result.isPresent(), is(equalTo(false)));
    }
}