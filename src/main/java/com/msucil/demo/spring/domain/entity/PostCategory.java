package com.msucil.demo.spring.domain.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Sushil Ale
 * @since 9/8/2017
 */

@Entity
@Table(name = "post_category")
@NoArgsConstructor
@Getter
@Setter
public class PostCategory extends Category {

    private static final long serialVersionUID = 400865442060373026L;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "postCategory")
    private List<Post> posts = new ArrayList<>();
}