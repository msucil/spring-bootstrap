package com.msucil.demo.spring.repository;

import com.msucil.demo.spring.domain.entity.PostCategory;

/**
 * @author Sushil Ale
 * @since 9/8/2017
 */
public interface PostCategoryDao extends BaseDao<PostCategory, Long> {

}
