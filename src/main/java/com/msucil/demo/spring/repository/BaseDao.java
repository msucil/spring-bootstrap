package com.msucil.demo.spring.repository;

import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

/**
 * Commaon interface for all Dao
 * Implemented by BaseDaoImpl
 *
 * @param <T>
 *     entity
 * @param <E>
 *     primary key type
 *
 * @author Sushil Ale
 * @since 8/6/2017
 */
public interface BaseDao<T, E> {

    /**
     * Save new entity
     *
     * @param entity
     *     of type T
     *
     * @return T instance of entity T
     */
    T save(@Valid T entity);

    /**
     * Update exisiting entity
     *
     * @param entity
     *     instance of entity T
     *
     * @return T instance of entity T
     */
    T update(@Valid T entity);

    /**
     * Remove entity of type T
     *
     * @param entity
     *     T to be removed from the database
     */
    void delete(@Valid T entity);

    /**
     * Removes entity based on the given primary key
     *
     * @param id
     *     primary key of type E
     */
    void deleteById(E id);

    /**
     * Returns Entity of type T based on given primary key
     *
     * @param id
     *     primary key of type E
     *
     * @return T instance of entity T
     */
    T find(E id);

    void detach(@Valid T entity);

    /**
     * Retuns Single Result on the basis of given attribute based parameter
     * Currently use AND and equal operator for creating query conditions
     *
     * @param restrictionParameters
     *     prameter map with attribute name and respective value
     *
     * @return Optional of type T or null
     */
    Optional<T> findOneBy(Map<String, Object> restrictionParameters);

    /**
     * Create QueryResult of type T based on sorting parameter, offset and page size
     *
     * @param orderBy
     *     map  for sorting the result
     * @param page
     *     current page no ie. offset for query
     * @param pageSize
     *     no of items to be returned
     *
     * @return QueryResult of type T which is then executed to fetch result
     */
    QueryResult<T> findAll(Map<String, SortOrder> orderBy, int page, int pageSize);
}
