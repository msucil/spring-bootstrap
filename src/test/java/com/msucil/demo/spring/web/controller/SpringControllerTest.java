package com.msucil.demo.spring.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author Sushil Ale
 * @since 10/15/2017
 */

@WebAppConfiguration
public class SpringControllerTest {

    @InjectMocks
    private SpringController springController;

    private MockMvc mvc;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);

        mvc = MockMvcBuilders.standaloneSetup(springController).build();
    }

    @Test
    public void testIndexPageReturnsJspPage() throws Exception{
        mvc.perform(get("/"))
            .andExpect(status().isOk())
            .andExpect(view().name("index"));
    }

    @Test
    public void testThymeleafPageReturnsThymeleafPage() throws Exception{
        mvc.perform(get("/thymeleaf"))
            .andExpect(status().isOk())
            .andExpect(view().name("thpage"));
    }
}