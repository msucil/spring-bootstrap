package com.msucil.demo.spring.repository;

import org.springframework.stereotype.Repository;

import com.msucil.demo.spring.domain.entity.PostCategory;

/**
 * @author Sushil Ale
 * @since 9/8/2017
 */

@Repository
public class PostCategoryDaoImpl extends BaseDaoImpl<PostCategory, Long> implements PostCategoryDao {

    public PostCategoryDaoImpl() {
        setClazz(PostCategory.class);
    }
}