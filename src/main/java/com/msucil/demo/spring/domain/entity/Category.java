package com.msucil.demo.spring.domain.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import com.msucil.demo.spring.domain.constant.Status;
import org.hibernate.annotations.Type;

/**
 * @author Sushil Ale
 * @since 9/8/2017
 */
@Getter
@Setter
@MappedSuperclass
public abstract class Category implements Serializable {

    private static final long serialVersionUID = 2684702136779518243L;

    @Column(name = "id",  nullable = false, updatable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private @NotBlank @NotEmpty String name;

    @Column(name = "slug", nullable = false)
    private String slug;

    @Column(name = "description")
    @Type(type = "text")
    private String description;

    @Enumerated(EnumType.STRING)
    private @NotNull Status status;

    @Column(name = "created_at", updatable = false, nullable = false)
    private LocalDate createdAt;

    @Column(name = "created_by", nullable = false, updatable = false)
    private Long createdBy;

    @Column(name = "version")
    @Version
    private Integer version;
}
