package com.msucil.demo.spring.repository;

import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

/**
 * BaseDao which needs to be extended by all the DAO of entity
 * Implements BaseDao which includs common behaviour for all DAO
 *
 * @author Sushil Ale
 * @since 8/6/2017
 */
@Transactional
@Slf4j
public class BaseDaoImpl<T extends Serializable, E> implements BaseDao<T, E> {

    /**
     * Stores type of the entity for which thes DAO is used
     */
    private Class<T> clazz;

    @PersistenceContext
    EntityManager entityManager;

    void setClazz(final Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public T save(T entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public T update(T entity) {
        return entityManager.merge(entity);
    }

    @Override
    public void delete(T entity) {
        entityManager.remove(entity);
    }

    @Override
    public void deleteById(E id) {
        final T entity = find(id);
        delete(entity);
    }

    @Override
    public T find(E id) {
        return entityManager.find(clazz, id);
    }

    @Override
    public QueryResult<T> findAll(Map<String, SortOrder> orderBy, int page, int pageSize) {
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<T> criteria = builder.createQuery(clazz);
        final int offset = (page == 0) ? 0 : ((page - 1) * pageSize);

        final Root<T> root = criteria.from(clazz);

        criteria.select(root);

        if (null != orderBy) {
            setOrderCriteria(builder, criteria, root, orderBy);
        }

        final TypedQuery<T> typedQuery = entityManager.createQuery(criteria);
        typedQuery.setFirstResult(offset);

        if (pageSize != 0) {
            typedQuery.setMaxResults(pageSize);
        }

        final QueryResult<T> queryResult = new QueryResult<>();
        queryResult.setResult(typedQuery.getResultList());
        queryResult.setTotalCount(getResultCount());

        return queryResult;
    }

    @Override
    public Optional<T> findOneBy(Map<String, Object> restrictionParameters) {
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<T> criteria = builder.createQuery(clazz);
        final Root<T> root = criteria.from(clazz);

        setRestrictions(builder, criteria, root, restrictionParameters);

        try {
            return Optional.of(entityManager.createQuery(criteria).getSingleResult());
        } catch (NoResultException ex) {
            log.info(ex.getLocalizedMessage());
            return Optional.empty();
        }
    }

    @Override
    public void detach(T entity) {
        entityManager.detach(entity);
    }

    protected void setOrderCriteria(CriteriaBuilder builder, CriteriaQuery<T> criteriaQuery, Root<T> root, Map<String, SortOrder> orderBy) {
        for (Entry<String, SortOrder> entry : orderBy.entrySet()) {

            switch (entry.getValue()) {
                case ASC:
                    criteriaQuery.orderBy(builder.asc(root.get(entry.getKey())));
                    break;
                case DESC:
                    criteriaQuery.orderBy(builder.desc(root.get(entry.getKey())));
                    break;
                default:
                    throw new UnsupportedOperationException("Unsupported Sort order: " + entry.getValue().getValue());
            }
        }
    }

    /**
     * Add conditions with <code>WHERE</code> clause in existing builder
     * Does not support entity association parameter condition
     * Only add equal with AND operator.
     *
     * @param builder
     *     query builder on which restrictions are added
     * @param criteriaQuery
     *     typed criteria query used to generate JPA query
     * @param root
     *     entity on which restrictions are applied to
     * @param restrictionParameters
     *     contains property name as key and property value as value
     */
    protected void setRestrictions(CriteriaBuilder builder, CriteriaQuery<T> criteriaQuery, Root<T> root, Map<String, Object> restrictionParameters) {
        for (Entry<String, Object> entry : restrictionParameters.entrySet()) {
            criteriaQuery.where(builder.equal(root.get(entry.getKey()), entry.getValue()));
        }
    }

    /**
     * Returns total count of the query result
     *
     * @param resctrictions
     *     variable conditions used in the query.
     *     It may contain 0 or more restrictions
     *
     * @return Long total no of result
     *
     * @see Predicate
     */
    protected Long getResultCount(Predicate... resctrictions) {
        final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);

        final Root<T> root = criteriaQuery.from(clazz);
        criteriaQuery.where(resctrictions);

        criteriaQuery.select(builder.count(root));

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }
}
