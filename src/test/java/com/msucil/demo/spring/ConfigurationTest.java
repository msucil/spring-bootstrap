package com.msucil.demo.spring;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNot.not;

import javax.sql.DataSource;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.msucil.demo.spring.configuration.root.SpringRootConfig;
import com.msucil.demo.spring.repository.PostDao;
import org.testng.annotations.Test;

/**
 * @author Sushil Ale
 * @since 10/12/2017
 */
public class ConfigurationTest {

    @Test(enabled = false)
    public void testSpringRootConfigurationReturnsRegisteredBeans() {
        final AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

        applicationContext.register(SpringRootConfig.class);
        applicationContext.refresh();

        final DataSource dataSource = (DataSource) applicationContext.getBean("dataSource");

        final PostDao postDao = (PostDao) applicationContext.getBean("postDaoImpl");

        assertThat(dataSource, is(not(nullValue())));
        assertThat(postDao, is(not(nullValue())));
    }

}
