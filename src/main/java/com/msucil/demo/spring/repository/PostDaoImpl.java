package com.msucil.demo.spring.repository;

import org.springframework.stereotype.Repository;

import com.msucil.demo.spring.domain.entity.Post;

/**
 * @author Sushil Ale
 * @since 7/18/2017
 */
@Repository
public class PostDaoImpl extends BaseDaoImpl<Post, Long> implements PostDao {

    public PostDaoImpl() {
        setClazz(Post.class);
    }

}
