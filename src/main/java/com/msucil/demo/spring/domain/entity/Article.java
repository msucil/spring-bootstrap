package com.msucil.demo.spring.domain.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import com.msucil.demo.spring.domain.constant.Status;
import org.hibernate.annotations.Type;

/**
 * @author Sushil Ale
 * @since 7/10/2017
 */

@Getter
@Setter
@MappedSuperclass
abstract class Article implements Serializable {
    private static final long serialVersionUID = -1851667731625171272L;

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, name = "title", unique = true)
    private @NotBlank @NotEmpty String title;

    @Column(name = "slug", nullable = false, unique = true)
    private String slug;

    @Column(name = "content")
    @Type(type = "text")
    private String content;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private @NotNull Status status;

    @Column(name = "version")
    @Version
    private Integer version;

    @Column(name = "created_by", updatable = false, nullable = false)
    private Long createdBy;

    @Column(name = "created_at", updatable = false, nullable = false)
    private LocalDate createdAt;

    @Column(name = "updated_by", insertable = false)
    private Long updatedBy;

    @Column(name = "updated_at", insertable = false)
    private LocalDate updatedAt;
}
